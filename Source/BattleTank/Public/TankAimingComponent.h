// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"


// Enum for aiming state
UENUM()
enum class EAimingState : uint8
{
	Reloading,
	Aiming,
	Ready,
	OutOfAmmo
};

/*Forward Declaration, with this I don't need to #include a lot of header files which can take longer to compile.
Do This on your header files only, CPP files may have includes as you will need to inherit the methods*/

class AProjectile;
class UTankBarrel;
class UTurret;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	virtual void BeginPlay() override;

	// Sets default values for this component's properties
	UTankAimingComponent();

	void AimAt(FVector HitLocation);
	
	UFUNCTION(BlueprintCallable, Category = "Setup")
		void Fire();

	UFUNCTION(BlueprintCallable, Category = Setup)
		void Initialise(UTankBarrel* BarrelToSet, UTurret* TurretToSet);
	
	EAimingState GetAimingState() const;

	UFUNCTION (BlueprintCallable, Category = "Firing")
	int GetRoundsLeft() const;

	
protected:

	UPROPERTY(BlueprintReadOnly, Category = State)
		EAimingState AimingState = EAimingState::Reloading;

private:
	
	UTankBarrel* Barrel = nullptr;
	UTurret* Turret = nullptr;
	   
	void MoveBarrelTowards(FVector AimDirection);

	//EditDefaultsOnly allows you to change the item only in the "original" blueprint menu, not on the world.
	UPROPERTY(EditDefaultsOnly, Category = "Firing")
		float ReloadTimeInSeconds = 3;

	UPROPERTY(EditAnywhere, Category = "Setup")
		TSubclassOf<AProjectile> ProjectileBlueprint;	// I can set my projectile BP through this

	UPROPERTY(EditAnywhere, Category = "Firing")
		float LaunchSpeed = 4000.f;

	double LastFireTime = 0;

	FVector AimDirection;

	bool IsBarrelMoving();

	int RoundsLeft = 3;

};
