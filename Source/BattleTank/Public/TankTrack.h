// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

/**
 * TankTrack is used to set maximum driving force, and to apply forces to the tank.
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	UFUNCTION (BlueprintCallable, Category = "Input")
	void SetThrottle(float Throttle);
	
	// This is max force per track, in Newtons
	UPROPERTY(EditDefaultsOnly)
		float TrackMaxDrivingForce = 4500000.f; //Assume 40 tonne tank, and 1g acceleration

	
	void ApplySidewaysForce();

	void DriveTrack();

	virtual void BeginPlay() override;

private:
	UTankTrack();

	UFUNCTION()
	void OnHit
	(
		UPrimitiveComponent* HitComponent, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComponent, 
		FVector NormalImpulse,
		const FHitResult& Hit
	);

	float CurrentThrottle = 0.f;
};
