// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "TankAimingComponent.h"


void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
		
}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
		
	auto AIAimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	
	if (!ensure(AIAimingComponent && PlayerPawn)) { return; }
		
			// Move towards the player
			MoveToActor(PlayerPawn, AcceptanceRadius);

			//Aim towards the player
			AIAimingComponent->AimAt(PlayerPawn->GetActorLocation());
		
			// if aim on aiming component equals Ready on Aiming State then Fire
			if (AIAimingComponent->GetAimingState() == EAimingState::Ready)
			{
				AIAimingComponent->Fire();
			}
	
	
}

//ATank* ATankAIController::GetAITank() const
//{
//	return Cast<ATank>(GetPawn());
//
//}
//
//ATank* ATankAIController::GetPlayerTank() const
//{
//	auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
//	if (!PlayerPawn) { return nullptr;  }
//	return Cast<ATank>(PlayerPawn);
//			
//}
