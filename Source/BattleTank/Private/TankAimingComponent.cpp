// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAimingComponent.h"
#include "TankBarrel.h"
#include "Turret.h"
#include "Math/Vector.h"
#include "Projectile.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/GameplayStaticsTypes.h"



// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true; 

	// ...
}

void UTankAimingComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	if (RoundsLeft <= 0)
	{
		AimingState = EAimingState::OutOfAmmo;
	}

	else if ((FPlatformTime::Seconds() - LastFireTime) < ReloadTimeInSeconds)
	{
		AimingState = EAimingState::Reloading;
	}
	else if (IsBarrelMoving())
	{
		AimingState = EAimingState::Aiming;
	}
	else
	{
		AimingState = EAimingState::Ready;
	}

}

void UTankAimingComponent::BeginPlay()
{
	//So the first fire is after initial reload
		LastFireTime = FPlatformTime::Seconds();
}

void UTankAimingComponent::Initialise(UTankBarrel* BarrelToSet, UTurret* TurretToSet)
{
	Barrel = BarrelToSet;
	Turret = TurretToSet;
}

EAimingState UTankAimingComponent::GetAimingState() const
{
	return AimingState;
}

int UTankAimingComponent::GetRoundsLeft() const //Getter Function
{
	return RoundsLeft;
}


void UTankAimingComponent::AimAt(FVector HitLocation)
{
	if (!ensure(Barrel)) { return; }
	FVector OutLaunchVelocity;
	FVector StartLocation = Barrel->GetSocketLocation(FName("FiringPos"));
	bool bHaveAimSolution = UGameplayStatics::SuggestProjectileVelocity
	(
		this,		//reference to self
		OutLaunchVelocity,
		StartLocation,
		HitLocation,
		LaunchSpeed,
		false,
		0.f,
		0.f,
		ESuggestProjVelocityTraceOption::DoNotTrace
	);
		if(bHaveAimSolution)
		{
		AimDirection = OutLaunchVelocity.GetSafeNormal();
		MoveBarrelTowards(AimDirection);
		
		}
		
		// No aim solution found
	
}

void  UTankAimingComponent::MoveBarrelTowards(FVector AimDirection)
{
	if (!ensure(Barrel || Turret)) { return; }
	//Work  out difference between current barrel rotation, and AimDirection
	auto BarrelRotator = Barrel->GetForwardVector().Rotation();
	auto AimAsRotator = AimDirection.Rotation();
	auto DeltaRotator = AimAsRotator - BarrelRotator;

	Barrel->Elevate(DeltaRotator.Pitch);

	// Rotates always on the shortest way
	if (FMath::Abs(DeltaRotator.Yaw) < 180.f)
	{
		Turret->Rotation(DeltaRotator.Yaw);
	}
	else
	{
		Turret->Rotation(-DeltaRotator.Yaw);
	}
}

void UTankAimingComponent::Fire()
{
	if (AimingState == EAimingState::Ready || AimingState == EAimingState::Aiming)
	// Spawn projectile and set the return of the spawn (which is the Projectile Blueprint var) as variable Projectile.
	{
		if (!ensure(Barrel)) { return; }
		if (!ensure(ProjectileBlueprint)) { return; }
		auto Projectile = GetWorld()->SpawnActor<AProjectile>(
			ProjectileBlueprint,
			Barrel->GetSocketLocation(FName("FiringPos")),
			Barrel->GetSocketRotation(FName("FiringPos"))
			);

		Projectile->Launch(LaunchSpeed);
		LastFireTime = FPlatformTime::Seconds();
		RoundsLeft--;
	}
}

bool UTankAimingComponent::IsBarrelMoving()
{
	if (!ensure(Barrel)) { return false; }
	auto BarrelForward = Barrel->GetForwardVector();
			
	return !BarrelForward.Equals(AimDirection, 0.01);
}