// Fill out your copyright notice in the Description page of Project Settings.

#include "Turret.h"

void UTurret::Rotation(float RelativeSpeedRotation)
{
	RelativeSpeedRotation = FMath::Clamp<float>(RelativeSpeedRotation, -1, 1);
	auto RotationChange = RelativeSpeedRotation * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	auto NewRotation = RelativeRotation.Yaw + RotationChange;
	
	SetRelativeRotation(FRotator(0, NewRotation, 0));
}


