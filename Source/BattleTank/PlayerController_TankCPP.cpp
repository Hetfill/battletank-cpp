// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerController_TankCPP.h"
#include "TankAimingComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Camera/PlayerCameraManager.h"


void APlayerController_TankCPP::BeginPlay()
{
	///Makes sure that begin play on Super classes is being called
	Super::BeginPlay();

	auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(AimingComponent)) { return; }
	{
		FoundAimingComponent(AimingComponent);
	}

}

void APlayerController_TankCPP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AimTowardsCrosshair();
}


void APlayerController_TankCPP::AimTowardsCrosshair()
{
	if (!GetPawn()) { return; } // e.g. if not possessing
	auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(AimingComponent)) { return; }
	
	FVector HitLocation;
	bool bGotHitLocation = GetSightRayHitLocation(HitLocation);
	
	if (bGotHitLocation)
	{	
		AimingComponent->AimAt(HitLocation);
	}
}

// Get world location of linetrace through crosshair, returns true if hits landscape
bool APlayerController_TankCPP::GetSightRayHitLocation(FVector& HitLocation) const
{
	//Find the crosshair position
	
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);
	auto ScreenLocation = FVector2D(ViewportSizeX * CrossHairXLocation, ViewportSizeY * CrossHairYLocation);

	// "De-Project" the screen position of the crosshair to a world direction
	FVector LookDirection;
	if (GetLookDirection(ScreenLocation, LookDirection))
	{
		return GetLookVectorHitLocation(LookDirection, HitLocation);
	}
	return false;
}

// "De-Project" the screen position of the crosshair to a world direction
bool APlayerController_TankCPP::GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const
{
	FVector CameraWorldLocation; //To be Discarded
		
	return DeprojectScreenPositionToWorld
	(
		ScreenLocation.X,
		ScreenLocation.Y,
		CameraWorldLocation,
		LookDirection
	);
}

bool APlayerController_TankCPP::GetLookVectorHitLocation(FVector LookDirection, FVector& HitLocation) const
{
	//Line-trace along that look direction, and see what we hit (up to max range)
	FHitResult HitResult;
	auto StartLocation = PlayerCameraManager->GetCameraLocation();
	auto EndLocation = StartLocation + (LookDirection * BarrelRange);
		
	if (GetWorld()->LineTraceSingleByChannel(
		HitResult,
		StartLocation,
		EndLocation,
		ECC_Visibility)
		)
	{
		HitLocation = HitResult.Location;

		return true;
	}

	return false; //Line trace failed
}