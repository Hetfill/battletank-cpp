// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerController_TankCPP.generated.h"		//Must be the last include

class UTankAimingComponent;

/*Responsible for helping the player aim.*/

UCLASS()
class BATTLETANK_API APlayerController_TankCPP : public APlayerController
{
	GENERATED_BODY()
	
	
protected:

	/// With BlueprintIpmplementableEvent there's no need for definition on cpp file
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
		void FoundAimingComponent(UTankAimingComponent* AimCompRef);
public:
		
	virtual void BeginPlay() override;			// override looks for the BeginPlay function on the PlayerController's Parents, 
													// you can use on different situations
													// when you see a class with a method declared virtual, 
														// that means you can override it in the future.
	virtual void Tick(float DeltaTime) override;

	bool GetSightRayHitLocation(FVector& OutHitLocation) const;

	UPROPERTY(EditAnywhere)
		float BarrelRange = 3000000.f;

private:
	// Start the tank moving the barrel so that a shot would it where
	// the crosshair intersects the world
	void AimTowardsCrosshair();
		

	UPROPERTY(EditDefaultsOnly)
		float CrossHairXLocation = 0.5;
	
	UPROPERTY(EditDefaultsOnly)
		float CrossHairYLocation = 0.33333;

	bool GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const;

	bool GetLookVectorHitLocation(FVector LookDirection, FVector& HitLocation) const;
};

